const Router = require('koa-router');
const ApiRoutes = require('./api');

const router = new Router();

router.use('/api', ApiRoutes.routes(), ApiRoutes.allowedMethods());

module.exports = router;
