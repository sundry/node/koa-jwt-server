const { expect } = require('chai');
const axios = require('axios');
const server = require('../../../../../../test/server');

describe('routes', () => {
  beforeEach(async () => {
    await server.start();
  });
  afterEach(async () => {
    await server.stop();
  });
  describe('api/v1/login', () => {
    it('should login', async () => {
      const options = {
        url: `http://localhost:3000/api/v1/login`,
        method: 'POST',
        data: {
          email: 'developer@test.com',
          password: 'abc123',
        },
      };
      const res = await axios(options);
      expect(res.status).to.equal(200);
      expect(res.data).to.have.property('token');
    });
    it('should login with bad credentials', async () => {
      const options = {
        url: `http://localhost:3000/api/v1/login`,
        method: 'POST',
        data: {
          email: 'test',
          password: 'test',
        },
        validateStatus: false,
      };
      const res = await axios(options);
      expect(res.status).to.equal(403);
    });
  });
});
