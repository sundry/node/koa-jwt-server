const Router = require('koa-router');

const router = new Router();

const login = async ctx => {
  const { Users } = ctx.db;
  const { email, password } = ctx.request.body;
  const user = await Users.findOne({
    where: {
      email,
    },
  });
  if (!user || !user.verifyPassword(password)) {
    const err = new Error('Email or Password is incorrect');
    err.status = 403;
    throw err;
  }
  const token = ctx.state.refreshJWT(user.id);
  ctx.body = {
    token,
  };
  ctx.status = 200;
};

router.post('/', login);

module.exports = router;
