const Router = require('koa-router');

const router = new Router();

const logout = ctx => {
  ctx.set('JWT_TOKEN', '');
  ctx.cookies.set('token', null);
  ctx.status = 200;
};

router.post('/', logout);

module.exports = router;
