const { expect } = require('chai');
const axios = require('axios');
const server = require('../../../../../../test/server');
const loginUser = require('../../../../../../test/loginUser');

describe('routes', () => {
  beforeEach(async () => {
    await server.start();
  });
  afterEach(async () => {
    await server.stop();
  });
  describe('api/v1/me', () => {
    it('should get me with query param', async () => {
      const user = await loginUser();
      expect(user.status).to.equal(200);
      expect(user.data).to.have.property('token');
      const token = user.data.token;
      const options = {
        url: `http://localhost:3000/api/v1/me?token=${token}`,
        method: 'GET',
      };
      const res = await axios(options);
      expect(res.status).to.equal(200);
      expect(res.data).to.have.property('email');
      expect(res.data.email).to.equal('developer@test.com');
    });
    it('should get me with header', async () => {
      const user = await loginUser();
      expect(user.status).to.equal(200);
      expect(user.data).to.have.property('token');
      const token = user.data.token;
      const options = {
        url: `http://localhost:3000/api/v1/me`,
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
      const res = await axios(options);
      expect(res.status).to.equal(200);
      expect(res.data).to.have.property('email');
      expect(res.data.email).to.equal('developer@test.com');
    });
    it('should get me with cookie', async () => {
      const user = await loginUser();
      expect(user.status).to.equal(200);
      expect(user.data).to.have.property('token');
      const token = user.data.token;
      const options = {
        url: `http://localhost:3000/api/v1/me`,
        method: 'GET',
        headers: {
          Cookie: `token=${token};`,
        },
      };
      const res = await axios(options);
      expect(res.status).to.equal(200);
      expect(res.data).to.have.property('email');
      expect(res.data.email).to.equal('developer@test.com');
    });
    it('should get me with no token', async () => {
      const options = {
        url: `http://localhost:3000/api/v1/me`,
        method: 'GET',
        validateStatus: false,
      };
      const res = await axios(options);
      expect(res.status).to.equal(401);
    });
  });
});
