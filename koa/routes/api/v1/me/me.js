const Router = require('koa-router');
const loggedInUser = require('../../../../middleware/loggedInUser');

const router = new Router();

const getMe = async ctx => {
  ctx.body = ctx.state.user;
  ctx.status = 200;
};

router.get('/', loggedInUser, getMe);

module.exports = router;
