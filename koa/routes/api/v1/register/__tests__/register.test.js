const { expect } = require('chai');
const axios = require('axios');
const server = require('../../../../../../test/server');

let verificationHash;

describe('routes', () => {
  beforeEach(async () => {
    await server.start();
  });
  afterEach(async () => {
    await server.stop();
  });
  describe('api/v1/register', () => {
    it('should register a new account and user', async () => {
      const options = {
        url: `http://localhost:3000/api/v1/register`,
        method: 'POST',
        data: {
          email: 'test@test.com',
          password: 'abc123',
        },
      };
      const res = await axios(options);
      expect(res.status).to.equal(200);
      expect(res.data).to.have.property('verificationHash');
      verificationHash = res.data.verificationHash;
    });
    it('should complete registration with the hash code', async () => {
      const options = {
        url: `http://localhost:3000/api/v1/register/complete/${verificationHash}`,
        method: 'GET',
      };
      const res = await axios(options);
      expect(res.status).to.equal(200);
      expect(res.data).to.have.property('token');
    });
    it('should error registration with bad hash code', async () => {
      const options = {
        url: `http://localhost:3000/api/v1/register/complete/test`,
        method: 'GET',
        validateStatus: false,
      };
      const res = await axios(options);
      expect(res.status).to.equal(404);
    });
    it('should error with bad email', async () => {
      const options = {
        url: `http://localhost:3000/api/v1/register`,
        method: 'POST',
        validateStatus: false,
      };
      const res = await axios(options);
      expect(res.status).to.equal(400);
    });
    it('should error with bad password', async () => {
      const options = {
        url: `http://localhost:3000/api/v1/register`,
        method: 'POST',
        data: {
          email: 'test',
        },
        validateStatus: false,
      };
      const res = await axios(options);
      expect(res.status).to.equal(400);
    });
    it('should error with duplicate user', async () => {
      const options = {
        url: `http://localhost:3000/api/v1/register`,
        method: 'POST',
        data: {
          email: 'test@test.com',
          password: 'abc123',
        },
        validateStatus: false,
      };
      const res = await axios(options);
      expect(res.status).to.equal(400);
    });
  });
});
