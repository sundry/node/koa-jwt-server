const Router = require('koa-router');

const router = new Router();

const register = async ctx => {
  const { Users, Accounts } = ctx.db;
  if (!ctx.request.body.email) {
    const err = new Error('email is required');
    err.status = 400;
    throw err;
  }
  if (!ctx.request.body.password) {
    const err = new Error('password is required');
    err.status = 400;
    throw err;
  }
  const existingUser = await Users.findOne({
    where: {
      email: ctx.request.body.email,
    },
  });
  if (existingUser) {
    const err = new Error(`${ctx.request.body.email} already exists`);
    err.status = 400;
    throw err;
  }
  const account = await Accounts.create();
  const data = { accountId: account.id, ...ctx.request.body };
  const user = await Users.scope('create').create(data);
  const email = ctx.email;
  email.register(user.email, user.verificationHash);
  ctx.body = { verificationHash: user.verificationHash };
  ctx.status = 200;
};

const completeRegistration = async ctx => {
  const { Users } = ctx.db;
  const { verificationHash } = ctx.params;
  const user = await Users.findOne({
    where: {
      verificationHash,
    },
  });
  if (!user) {
    const err = new Error(`${verificationHash} not found`);
    err.status = 404;
    throw err;
  }
  await user.update({
    verified: true,
    verificationHash: null,
  });
  const token = ctx.state.refreshJWT(user.id);
  ctx.body = { token };
  ctx.status = 200;
};

router.post('/', register);
router.get('/complete/:verificationHash', completeRegistration);

module.exports = router;
