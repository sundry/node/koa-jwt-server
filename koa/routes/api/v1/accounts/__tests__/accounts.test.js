const { expect } = require('chai');
const axios = require('axios');
const loginUser = require('../../../../../../test/loginUser');
const server = require('../../../../../../test/server');

describe('routes', () => {
  beforeEach(async () => {
    await server.start();
  });
  afterEach(async () => {
    await server.stop();
  });
  describe('api/v1/accounts', () => {
    it('should update developer account', async () => {
      const user = await loginUser();
      expect(user.status).to.equal(200);
      expect(user.data).to.have.property('token');
      const token = user.data.token;
      const options = {
        url: `http://localhost:3000/api/v1/accounts/1?token=${token}`,
        method: 'PUT',
        data: {
          id: 2,
          name: 'Developer Account 2',
        },
      };
      const res = await axios(options);
      expect(res.status).to.equal(200);
    });
    it('should give forbidden error trying to update anothers account', async () => {
      const user = await loginUser();
      expect(user.status).to.equal(200);
      expect(user.data).to.have.property('token');
      const token = user.data.token;
      const options = {
        url: `http://localhost:3000/api/v1/accounts/2?token=${token}`,
        method: 'PUT',
        data: {
          id: 3,
          name: 'Test Account 2',
        },
        validateStatus: false,
      };
      const res = await axios(options);
      expect(res.status).to.equal(403);
      expect(res.data).to.have.property('error');
      expect(res.data.error).to.have.property('message');
      expect(res.data.error.message).to.equal('Forbidden');
    });
  });
});
