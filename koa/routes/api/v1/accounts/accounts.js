const Router = require('koa-router');
const loggedInUser = require('../../../../middleware/loggedInUser');

const router = new Router();

const updateAccount = async ctx => {
  if (ctx.state.user.accountId !== Number(ctx.params.id)) {
    const err = new Error('Forbidden');
    err.status = 403;
    throw err;
  }
  const { Accounts } = ctx.db;
  await Accounts.scope('update').update(ctx.request.body, {
    where: {
      id: ctx.params.id,
    },
  });
  ctx.status = 200;
};

router.put('/:id', loggedInUser, updateAccount);

module.exports = router;
