const Router = require('koa-router');

const router = new Router();

const getHealthcheck = async ctx => (ctx.status = 200);

const getError = async () => {
  throw new Error('Not OK');
};

router.get('/', getHealthcheck);
router.get('/error', getError);

module.exports = router;
