const { expect } = require('chai');
const axios = require('axios');
const server = require('../../../../../../test/server');

describe('routes', () => {
  beforeEach(async () => {
    await server.start();
  });
  afterEach(async () => {
    await server.stop();
  });
  describe('api/v1/healthcheck', () => {
    it('should GET /api/v1/healthcheck', async () => {
      const res = await axios(`http://localhost:3000/api/v1/healthcheck`);
      expect(res.status).to.equal(200);
    });
    it('should return a 500', async () => {
      const options = {
        url: `http://localhost:3000/api/v1/healthcheck/error`,
        method: 'GET',
        validateStatus: false,
      };
      const res = await axios(options);
      expect(res.status).to.equal(500);
    });
  });
});
