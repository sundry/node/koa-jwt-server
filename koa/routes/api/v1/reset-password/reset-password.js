const Router = require('koa-router');

const router = new Router();

const resetPassword = async ctx => {
  const { Users } = ctx.db;
  const user = await Users.findOne({
    where: {
      email: ctx.request.body.email,
    },
  });
  if (!user) {
    const err = new Error('User not found');
    err.status = 404;
    throw err;
  }
  user.setResetPassword(ctx.request.body.timeout || 15);
  await user.save();
  ctx.email.resetPassword(user.email, user.resetPasswordHash);
  ctx.body = {
    resetPasswordHash: user.resetPasswordHash,
  };
  ctx.status = 200;
};

const verifyHash = async ctx => {
  const { Users } = ctx.db;
  const user = await Users.findOne({
    where: {
      resetPasswordHash: ctx.params.hash,
    },
  });
  if (!user) {
    const err = new Error('User not found');
    err.status = 404;
    throw err;
  }
  if (!user.hasValidResetPassword()) {
    const err = new Error('Verification has timed out');
    err.status = 400;
    throw err;
  }
  user.unsetResetPassword();
  user.save();
  ctx.status = 200;
};

router.put('/', resetPassword);
router.get('/verify/:hash', verifyHash);

module.exports = router;
