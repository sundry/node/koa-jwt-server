const Router = require('koa-router');
const healthcheck = require('./healthcheck');
const info = require('./info');
const login = require('./login');
const logout = require('./logout');
const me = require('./me');
const register = require('./register');
const accounts = require('./accounts');
const users = require('./users');
const resetPassword = require('./reset-password');

const router = new Router();

router.use('/healthcheck', healthcheck.routes(), healthcheck.allowedMethods());
router.use('/info', info.routes(), info.allowedMethods());
router.use('/login', login.routes(), login.allowedMethods());
router.use('/logout', logout.routes(), logout.allowedMethods());
router.use('/me', me.routes(), me.allowedMethods());
router.use('/register', register.routes(), register.allowedMethods());
router.use('/accounts', accounts.routes(), accounts.allowedMethods());
router.use('/users', users.routes(), users.allowedMethods());
router.use(
  '/reset-password',
  resetPassword.routes(),
  resetPassword.allowedMethods()
);

module.exports = router;
