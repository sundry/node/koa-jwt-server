const Router = require('koa-router');

const router = new Router();

const getInfo = async ctx => {
  ctx.body = {
    env: process.env.NODE_ENV,
    hostname: require('os').hostname(),
    tag: process.env.DOCKER_TAG,
  };
  ctx.status = 200;
};

router.get('/', getInfo);

module.exports = router;
