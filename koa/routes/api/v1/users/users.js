const Router = require('koa-router');
const loggedInUser = require('../../../../middleware/loggedInUser');

const router = new Router();

const updateUser = async ctx => {
  if (ctx.state.user.id !== Number(ctx.params.id)) {
    const err = new Error('Forbidden');
    err.status = 403;
    throw err;
  }
  const { Users } = ctx.db;
  await Users.scope('update').update(ctx.request.body, {
    where: {
      id: ctx.params.id,
    },
    individualHooks: true,
  });
  ctx.status = 200;
};

router.put('/:id', loggedInUser, updateUser);

module.exports = router;
