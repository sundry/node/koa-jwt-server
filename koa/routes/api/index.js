const Router = require('koa-router');
const v1Routes = require('./v1');

const router = new Router();

router.use('/v1', v1Routes.routes(), v1Routes.allowedMethods());

module.exports = router;
