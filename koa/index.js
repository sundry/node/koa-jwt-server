require('dotenv').config({ path: __dirname + '/../.env' });
const Koa = require('koa');
const cors = require('@koa/cors');
const bodyParser = require('koa-bodyparser');
const cookieParser = require('koa-cookie').default;
const koajwt = require('koa-jwt');
const jwt = require('jsonwebtoken');
const db = require('../db/postgres');
const email = require('../email');
const Routes = require('./routes');

const app = new Koa();

/**
 * Environment Variables
 */
const JWT_SECRET = process.env.JWT_SECRET;
const PORT = process.env.PORT;
const NODE_ENV = process.env.NODE_ENV;
const SESSION_TIMEOUT = process.env.SESSION_TIMEOUT;

app.context.db = db;
app.context.email = email;

app.use(async (ctx, next) => {
  await next();
  const rt = ctx.response.get('X-Response-Time');
  console.log(`${ctx.method} ${ctx.status} ${ctx.url} - ${rt}`);
});

app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.set('X-Response-Time', `${ms}ms`);
});

app.use(async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    ctx.status = err.status || 500;
    ctx.type = 'json';
    ctx.body = {
      error: {
        message: err.message,
      },
    };
    ctx.app.emit('error', err, ctx);
  }
});

// Cors
app.use(
  cors({
    origin: '*',
    credentials: true,
  })
);

app.use(bodyParser());
app.use(cookieParser());

/**
 * JWT Authentication
 */
app.use(
  koajwt({
    secret: JWT_SECRET,
    getToken(ctx) {
      if (
        ctx.headers.authorization &&
        ctx.headers.authorization.split(' ')[0] === 'Bearer'
      ) {
        return ctx.headers.authorization.split(' ')[1];
      } else if (ctx.request.query && ctx.request.query.token) {
        return ctx.request.query.token;
      } else if (ctx.cookie && ctx.cookie.token) {
        return ctx.cookie.token;
      }
      return null;
    },
  }).unless({
    path: [
      '/test',
      '/api/v1/healthcheck',
      '/api/v1/healthcheck/error',
      '/api/v1/info',
      '/api/v1/login',
      '/api/v1/logout',
      '/api/v1/register',
      '/api/v1/register/complete',
      /\/api\/v1\/register\/complete\//i,
      '/api/v1/reset-password',
      /\/api\/v1\/reset-password\//i,
    ],
  })
);

/**
 * Refresh JWT for each use
 */
app.use(async (ctx, next) => {
  ctx.state.refreshJWT = id => {
    const token = jwt.sign({ id, createdAt: new Date() }, JWT_SECRET, {
      expiresIn: SESSION_TIMEOUT,
    });
    const options = {
      maxAge: SESSION_TIMEOUT,
      httpOnly: false,
    };
    ctx.set('JWT_TOKEN', token);
    ctx.cookies.set('token', token, options);
    return token;
  };
  if (ctx.state.user) {
    ctx.state.refreshJWT(ctx.state.user.id);
  }
  await next();
});

// Routes
app.use(Routes.routes()).use(Routes.allowedMethods());

app.on('error', (err, ctx) => {
  console.error(err, ctx);
});

const server = app.listen(PORT);
server.on('listening', () => {
  console.log(
    `Koa Server started on Port: ${PORT} | Environment : ${NODE_ENV}`
  );
});

module.exports = {
  app,
  server,
};
