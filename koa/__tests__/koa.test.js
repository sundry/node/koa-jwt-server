const { expect } = require('chai');
const axios = require('axios');
const server = require('../../test/server');

describe('express', () => {
  beforeEach(async () => {
    await server.start();
  });
  afterEach(async () => {
    await server.stop();
  });
  it('should return a 404', async () => {
    const options = {
      url: `http://localhost:3000/test`,
      method: 'GET',
      validateStatus: false,
    };
    const res = await axios(options);
    expect(res.status).to.equal(404);
  });
});
