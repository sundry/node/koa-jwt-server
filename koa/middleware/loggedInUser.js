module.exports = async (ctx, next) => {
  const { Users, Accounts } = ctx.db;
  ctx.state.user = await Users.scope('read').findByPk(ctx.state.user.id, {
    include: [
      {
        model: Accounts,
        as: 'account',
      },
    ],
  });
  await next();
};
