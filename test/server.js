let server;
module.exports = {
  start: async () => {
    delete require.cache[require.resolve('../koa')];
    server = require('../koa').server;
    const startServer = new Promise(resolve => server.on('listening', resolve));
    await new Promise(resolve => resolve(startServer));
  },
  stop: async () => {
    await server.close();
  },
};
