/* eslint-disable no-undef */

// Mocks
const mockery = require('mockery');
const nodemailerMock = require('nodemailer-mock');

// Enable mockery to mock objects
mockery.enable({
  warnOnUnregistered: false,
});

/* Once mocked, any code that calls require('nodemailer') 
will get our nodemailerMock */
mockery.registerMock('nodemailer', nodemailerMock);

const { spawn } = require('child_process');

const dbUp = async () => {
  console.log('db up');
  await new Promise(resolve => {
    const createDbs = spawn('npm', ['run', 'sequelize', 'db:create'], {
      stdio: 'inherit',
    });
    createDbs.on('close', (code, signal) => {
      resolve();
    });
  });
  await new Promise(resolve => {
    const migrateUp = spawn('npm', ['run', 'sequelize', 'db:migrate'], {
      stdio: 'inherit',
    });
    migrateUp.on('close', (code, signal) => {
      resolve();
    });
  });
  await new Promise(resolve => {
    const migrateUp = spawn('npm', ['run', 'sequelize', 'db:seed:all'], {
      stdio: 'inherit',
    });
    migrateUp.on('close', (code, signal) => {
      resolve();
    });
  });
};

const dbDown = async () => {
  console.log('db down');
  await new Promise(resolve => {
    const migrateUp = spawn('npm', ['run', 'sequelize', 'db:seed:undo:all'], {
      stdio: 'inherit',
    });
    migrateUp.on('close', (code, signal) => {
      resolve();
    });
  });
  await new Promise(resolve => {
    const migrateUp = spawn(
      'npm',
      ['run', 'sequelize', 'db:migrate:undo:all'],
      {
        stdio: 'inherit',
      }
    );
    migrateUp.on('close', (code, signal) => {
      resolve();
    });
  });
  await new Promise(resolve => {
    const cleanDb = spawn('npm', ['run', 'sequelize', 'db:drop'], {
      stdio: 'inherit',
    });
    cleanDb.on('close', (code, signal) => {
      resolve();
    });
  });
};

before(async () => {
  await dbDown();
  await dbUp();
});

after(async () => {
  mockery.deregisterAll();
  mockery.disable();
  await new Promise(resolve => setTimeout(resolve, 10000));
  await dbDown();
});
